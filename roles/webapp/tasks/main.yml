---

- name: Install dependencies
  package:
    name:
      - zip
      - unzip
      - python3
      - python3-pip
      - python3-virtualenv
      - pipenv
    state: present

- name: Create src folder
  become: yes
  file:
    path: /src
    state: directory
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0755

- name: Remove webapp archive file from src folder
  become: yes
  file:
    path: "/src/{{ webapp_root_name }}.zip"
    state: absent

- name: Remove webapp folder from src folder
  become: yes
  file:
    path: "/src/{{ webapp_root_name }}/"
    state: absent

- name: Create git archive from repo
  become: yes
  git:
    repo: "{{ webapp_git_repo_url }}"
    dest: "/src/{{ webapp_root_name }}"
    archive: "/src/{{ webapp_root_name }}.zip"
    version: "{{ webapp_git_branch_name }}"

- name: Remove gis_webapp folder
  file:
    path: "{{ webapp_root_directory_full_path }}"
    state: absent

- name: Create gis_webapp folder
  file:
    path: "{{ webapp_root_directory_full_path }}"
    state: directory
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0755

- name: Copy git archive to Nginx document root
  become: yes
  copy:
    src: "/src/{{ webapp_root_name }}.zip"
    dest: "{{ webapp_root_directory_full_path }}/{{ webapp_root_name }}.zip"
    remote_src: yes
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0644

- name: Extract gis_webapp.zip into /usr/share/nginx/html/gis_webapp/
  become: yes
  unarchive:
    src: "{{ webapp_root_directory_full_path }}/{{ webapp_root_name }}.zip"
    dest: "{{ webapp_root_directory_full_path }}/"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    remote_src: yes

- name: Remove webapp archive file
  file:
    path: "{{ webapp_root_directory_full_path }}/{{ webapp_root_name }}.zip"
    state: absent

- name: Create Ngnix site conf file from template
  template:
    src: django.conf.j2
    dest: "{{ webapp_config_path }}"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0644
  notify: Restart service nginx

- name: Create uwsgi_params file from template
  template:
    src: uwsgi_params.j2
    dest: "{{ webapp_directory_full_path }}/uwsgi_params"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0644
  notify: Restart service nginx

- name: Create gis_webapp_project_uwsgi.ini file from template
  template:
    src: "{{ webapp_project_name }}_uwsgi.ini.j2"
    dest: "{{ webapp_directory_full_path }}/{{ webapp_project_name }}_uwsgi.ini"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0644
  notify: Restart service nginx

- name: Set gis_webapp folder permissions
  file:
    path: "{{ webapp_root_directory_full_path }}"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    recurse: yes
    state: directory

- name: Check venv folder stats
  stat:
    path: "{{ webapp_root_directory_full_path }}/.venv/"
  register: venv_folder_stats

- name: Remove pipenv environment
  command:
    cmd: pipenv --three --rm
    chdir: "{{ webapp_root_directory_full_path }}/"
  environment:
    PIPENV_VENV_IN_PROJECT: 1
  when: venv_folder_stats.stat.exists

- name: Set Pipfile.lock file permissions
  file:
    path: "{{ webapp_root_directory_full_path }}/Pipfile.lock"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775

- name: Create pipenv environment and install dependencies
  command:
    cmd: pipenv --three install --dev
    chdir: "{{ webapp_root_directory_full_path }}/"
  environment:
    PIPENV_VENV_IN_PROJECT: 1

- name: Set .venv folder permissions
  file:
    path: "{{ webapp_root_directory_full_path }}/.venv/"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    recurse: yes
    state: directory

- name: Collect Django static files
  command:
    cmd: pipenv run python3 manage.py collectstatic --no-input --clear
    chdir: "{{ webapp_directory_full_path }}/"
  environment:
    PIPENV_VENV_IN_PROJECT: 1
    DJANGO_SETTINGS_MODULE: "{{ webapp_project_name }}.{{ django_settings_module }}"

- name: Set static files folder permissions
  file:
    path: "{{ webapp_directory_full_path }}/{{ webapp_project_name }}/static/"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    recurse: yes
    state: directory

- name: Create gis_webapp_project.service file from template
  template:
    src: "{{ webapp_project_name }}.service.j2"
    dest: "{{ systemd_services_path }}/{{ webapp_project_name }}.service"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
  notify: Restart service nginx

- name: Force all notified handlers to run at this point, not waiting for normal sync points
  meta: flush_handlers

- name: Create log files folder
  file:
    path: "{{ webapp_log_directory }}/"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    state: directory

- name: Create pid files folder
  file:
    path: "{{ webapp_run_directory }}/"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    state: directory

- name: Create log file and set permissions
  file:
    path: "{{ webapp_log }}"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    state: touch

- name: Set pid file permissions
  file:
    path: "{{ webapp_pid }}"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    state: touch

- name: Create vassals directory
  file:
    path: "{{ uwsgi_emperor_vassals_config_directory }}/"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    recurse: yes
    state: directory

- name: Create a symbolic link to vassals
  file:
    src: "{{ uwsgi_ini_file_path }}"
    dest: "{{ uwsgi_symlink_ini_file_path }}"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    state: link

- name: Set socket file permissions
  file:
    path: "{{ webapp_socket }}"
    owner: "{{ webapp_owner }}"
    group: "{{ webapp_group }}"
    mode: 0775
    state: touch

- name: Restart service gis_webapp_project
  systemd:
    name: "{{ webapp_project_name }}.service"
    state: restarted
    enabled: yes
    daemon_reload: yes
  environment:
    PIPENV_VENV_IN_PROJECT: 1
  notify: Restart service nginx

- name: Execute Django migrations
  command:
    cmd: pipenv run python3 manage.py migrate
    chdir: "{{ webapp_directory_full_path }}/"
  environment:
    PIPENV_VENV_IN_PROJECT: 1
    DJANGO_SETTINGS_MODULE: "{{ webapp_project_name }}.{{ django_settings_module }}"

#- name: Create Django superuser
#  command:
#    cmd: pipenv run python3 manage.py createsuperuser --username admin --email c_hueser@web.de
#    chdir: "{{ webapp_directory_full_path }}/"
#  environment:
#    PIPENV_VENV_IN_PROJECT: 1
#    DJANGO_SETTINGS_MODULE: "{{ webapp_project_name }}.{{ django_settings_module }}"

...
