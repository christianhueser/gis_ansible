
.DEFAULT_GOAL := build
.PHONY: build lint roles clean clean.roles

build: pipenv roles

pipenv:
		pipenv install --dev

roles:
		pipenv run ansible-galaxy role install -f -r requirements.yml -p roles/external/

lint:
		pipenv run reuse lint
		pipenv run ansible-lint -v --force-color .
		pipenv run yamllint --strict --format colored .

clean: clean.roles clean.pipenv

clean.pipenv:
		pipenv --rm

clean.roles:
		for ROLE in $$(grep 'name:' requirements.yml | cut -f5 -d" "); do \
				pipenv run ansible-galaxy role remove $$ROLE -p roles/external/ ;\
		done
